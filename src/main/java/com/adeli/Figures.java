package com.adeli;

public class Figures {
    private Figures() {
    }

    public static void show(int counts, boolean additive, char charOdd, char charEven, char charSpace, boolean normal) {
        if (normal) {
            if (charOdd == charEven) {
                if (additive)
                    for (int i = 1; i <= counts; i++)
                        xox(counts, charOdd, charSpace, i);
                else
                    for (int i = 1; i <= counts; i++)
                        yoy(counts, charOdd, charSpace, i);
            } else {
                if (additive) {
                    for (int i = 1; i <= counts; i++)
                        if (i % 2 != 0)
                            xox(counts, charOdd, charSpace, i);
                        else
                            xox(counts, charEven, charSpace, i);
                } else {
                    for (int i = 1; i <= counts; i++)
                        if (i % 2 != 0)
                            yoy(counts, charOdd, charSpace, i);
                        else
                            yoy(counts, charEven, charSpace, i);
                }
            }
        } else {  // Abnormal
            if (charOdd == charEven) {
                if (additive)
                    for (int i = 1; i <= counts; i++)
                        oxo(counts, charOdd, charSpace, i);
                else
                    for (int i = 1; i <= counts; i++)
                        oyo(counts, charOdd, charSpace, i);
            } else {
                if (additive) {
                    for (int i = 1; i <= counts; i++)
                        if (i % 2 != 0)
                            oxo(counts, charOdd, charSpace, i);
                        else
                            oxo(counts, charEven, charSpace, i);
                } else {
                    for (int i = 1; i <= counts; i++)
                        if (i % 2 != 0)
                            oyo(counts, charOdd, charSpace, i);
                        else
                            oyo(counts, charEven, charSpace, i);
                }
            }
        }
    }

    private static void xox(int counts, char charMain, char charSpace, int i) {
        com.adeli.Print.printChar(charMain, i, false);
        com.adeli.Print.printChar(charSpace, counts - i, true);
    }

    private static void oxo(int counts, char charMain, char charSpace, int i) {
        com.adeli.Print.printChar(charSpace, counts - i, false);
        com.adeli.Print.printChar(charMain, i, true);
    }

    private static void yoy(int counts, char charMain, char charSpace, int i) {
        com.adeli.Print.printChar(charMain, counts + 1 - i, false);
        com.adeli.Print.printChar(charSpace, i - 1, true);
    }

    private static void oyo(int counts, char charMain, char charSpace, int i) {
        com.adeli.Print.printChar(charSpace, i - 1, false);
        com.adeli.Print.printChar(charMain, counts + 1 - i, true);
    }

    // -----------------------------------------------------------
    public static void show1(int counts) {
        com.adeli.Print.printChar('*', counts, true);
    }

    public static void show2(int counts) {
        com.adeli.Print.printChar('*', counts, false);
    }

    public static void show3(int counts) {
        show(counts, true, '*', '*', ' ', true);
    }

    public static void show4(int counts) {
        show(counts, false, '*', '*', ' ', true);
    }

    public static void show5(int counts) {
        for (int i = 1; i <= counts; i++) {
            com.adeli.Print.printChar('*', 1, false);
            com.adeli.Print.printChar('-', i, false);
        }
    }

    public static void show6(int counts) {
        show3(counts - 1);
        show2(counts);
        show4(counts - 1);
    }

    public static void show7(int counts) {
        show(counts, false, '*', '*', ' ', false);
    }

    public static void show8(int counts) {
        show(counts - 1, true, '*', '-', ' ', true);
        show2(counts);
        show(counts - 1, false, '-', '*', ' ', true);
    }


    public static void show9(int counts) {
        show(counts, false, '*', '-', '$', false);
    }

    public static void show10(int counts) {
        for (int i = 1; i <= counts; i++) {
            com.adeli.Print.printChar('*', i, false);
            com.adeli.Print.printChar('-', 7 - (i * 2), false);
            com.adeli.Print.printChar('*', i, true);
        }
    }

    public static void show11(int counts) {
        show(counts, true, '*', '*', '-', true);
        show(counts, false, '*', '*', '-', true);
    }

    public static void show12(int counts) {
        show(counts, true, '*', '*', '-', false);
        show(counts, false, '*', '*', '-', false);
    }

}
