package com.adeli;

public class Print {
    private Print() { }

    public static void printChar(char ch, int n, boolean nextLine) {
        for (int i = 0; i < n; i++) {
            System.out.print(ch);
        }
        if (nextLine)
            System.out.println();

    }
}
